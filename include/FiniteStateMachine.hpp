#pragma once

#include <functional>
#include <list>
#include <map>
#include <memory>
#include <typeindex>
#include <typeinfo>
#include <tuple>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <thread>

#include "DispatchQueue.hpp"

namespace utility404 {

	template <class event,
	typename std::enable_if<std::is_polymorphic<event>::value, int>::type = 0>
	class State;

	/**
	 * @brief      Class for state machine.
	 */
	template <class event,
	typename std::enable_if<std::is_polymorphic<event>::value, int>::type = 0>
	class SubStateMachine {
	public:
		using action = std::function<void(std::shared_ptr<event> e)>; ///< Type of the action functions
		using guard = std::function<bool(std::shared_ptr<event> e)>; ///< Type of the guard functions

	public:
		/**
		 * @brief      Constructs the object.
		 */
		SubStateMachine()
		{
			m_InitialState = nullptr;
			m_CurrentState = nullptr;
		}
		SubStateMachine(const SubStateMachine&) = delete;
		SubStateMachine(SubStateMachine&&) = delete;
		SubStateMachine& operator=(SubStateMachine &&) = delete;

		/**
		 * @brief      Adds a state to the state machine.
		 *
		 * @param      s     the state to add
		 */
		void addState(State<event> *s)
		{
			m_States.push_back(std::unique_ptr<State<event>>(s));
		}
		/**
		 * @brief      Sets the initial state of the state machine.
		 *
		 * @param      initial  The initial state to set
		 */
		void setInitialState(State<event> *initial)
		{
			m_InitialState = initial;
			m_CurrentState = initial;
		}

		/**
		 * @brief      Process a given event. It means, check if the current
		 *             state has a transition associated to the given event
		 *
		 * @param[in]  e     the input event
		 */
		virtual bool processEvent(std::shared_ptr<event> e)
		{
			bool ret = false;
			if (m_CurrentState != nullptr) {
				State<event> *res = m_CurrentState->processEventState(e);
				if (res != nullptr){
					if (res != m_CurrentState)
					{
						m_CurrentState = res;
						m_CurrentState->onEntryInternal(e);
					}
					ret = true;
				}
			}
			return ret;
		}

	protected:
		std::list<std::unique_ptr<State<event>>>	m_States; ///< List of the states added to the state machine
		State<event>								*m_CurrentState; ///< Current state of the state machine
		State<event>								*m_InitialState; ///< Initial state of the state machine

	};

	/**
	 * @brief      Class for state machine with an internal dispatch queue.
	 */
	template <class event,
	typename std::enable_if<std::is_polymorphic<event>::value, int>::type = 0>
	class StateMachine : public SubStateMachine<event> {
	public:
		/**
		 * @brief      Constructs the object.
		 */
		StateMachine() {}
		/**
		 * @brief      Destroys the object.
		 */
		~StateMachine() {}

		/**
		 * @brief      Process a given event. It means, check if the current
		 *             state has a transition associated to the given event
		 *
		 * @param[in]  e     the input event
		 */
		virtual bool processEvent(std::shared_ptr<event> e)
		{
			std::unique_lock<std::mutex> lock(m_Lock);
			if (m_Filter and m_Filter(e)) {
				return false;
			} 
			bool ret = SubStateMachine<event>::processEvent(e);
			if (!ret and m_DefaultAction)
			{
				m_DefaultAction(e);
			}
			lock.unlock();
			return ret;
		}

		/**
		 * @brief      Sets the default action to execute if no transition is found.
		 *
		 * @param[in]  a     The default action to execute if no transition is found
		 */
		void setDefaultAction(typename SubStateMachine<event>::action a)
		{
			m_DefaultAction = a;
		}

		/**
		 * @brief      Sets the default action to execute if no transition is found.
		 *
		 * @param[in]  a     The default action to execute if no transition is found
		 */
		void setFilter(typename SubStateMachine<event>::guard f)
		{
			m_Filter = f;
		}

	protected:
		typename SubStateMachine<event>::action		m_DefaultAction; ///< Default action to execute if no transition is found
		typename SubStateMachine<event>::guard		m_Filter; ///< Function that filter the input events
		std::mutex									m_Lock; ///< Mutex for thread safety
	};

	/**
	 * @brief      Class for state machine with an internal dispatch queue.
	 */
	template <class event,
	typename std::enable_if<std::is_polymorphic<event>::value, int>::type = 0>
	class ThreadedStateMachine : public StateMachine<event> {
	public:
		/**
		 * @brief      Constructs the object.
		 */
		ThreadedStateMachine() {}
		/**
		 * @brief      Destroys the object.
		 */
		~ThreadedStateMachine() {}

		/**
		 * @brief      Process a given event. It means, check if the current
		 *             state has a transition associated to the given event
		 *
		 * @param[in]  e     the input event
		 */
		virtual bool processEvent(std::shared_ptr<event> e)
		{
			m_Queue.dispatch([e, this](){StateMachine<event>::processEvent(e);});
			return true;
		}

	private:
		DispatchQueue m_Queue; ///< Internal dispatch queue
	};

	/**
	 * @brief      Class for state.
	 *
	 *             States can be added to a state machine. They hold their
	 *             transitions to other states. As state inherits from state
	 *             machine, they can also hold sub state. If no transition is
	 *             found for this state when an event is received, the event
	 *             will be forwarded to the current substate.
	 */
	template <class event,
	typename std::enable_if<std::is_polymorphic<event>::value, int>::type>
	class State : public SubStateMachine<event> {
		friend class SubStateMachine<event>;
	public:
		/**
		 * @brief      Constructs the object.
		 */
		State(bool memorize = false)
		{
			m_Memorize = memorize;
		}

		/**
		 * @brief      Process a given event. It means, check if a transition is
		 *             associated to the given event. If not, the event will be
		 *             forwarded to the derived state machine of this state.
		 *
		 * @param[in]  e     the input event
		 *
		 * @return     The new state if a transition was found.
		 */
		virtual State* processEventState(std::shared_ptr<event> e)
		{
			event *ev = e.get();
			if(m_Transitions.count(std::type_index(typeid(*ev))) != 0) {
				for (auto t : m_Transitions[std::type_index(typeid(*ev))])
				{
					if ( std::get<2>(t) &&
						!std::get<2>(t)(e) ) {
						continue;
					}
					if (std::get<0>(t) != this)
					{
						onExitInternal(e);
					}
					if (std::get<1>(t))
					{
						std::get<1>(t)(e);
					}
					return std::get<0>(t);
				}
			}
			if (SubStateMachine<event>::processEvent(e))
			{
				return this;
			}
			else
			{
				return nullptr;
			}
		}

		/**
		 * @brief      Function to execute when this state is entered
		 *
		 *             By default, do nothing. But you can create a class that
		 *             inherits from state and overload this function.
		 *
		 * @param[in]  e     The event that lead to this state entry.
		 */
		virtual void onEntry(std::shared_ptr<event> e) {}
		/**
		 * @brief      Function to execute when this state is exited
		 *
		 *             By default, do nothing. But you can create a class that
		 *             inherits from state and overload this function.
		 *
		 * @param[in]  e     The event that lead to this state exit.
		 */
		virtual void onExit(std::shared_ptr<event> e) {}

		/**
		 * @brief      Adds a transition from this state to an other.
		 *
		 * @param      s          The destination state of the transition
		 * @param[in]  a          Optional action to perform on transition
		 * @param[in]  g          Optional guard function. Take the event as a
		 *                        parameter and tell if the transition is valid.
		 *
		 * @tparam     EventType  The event that can causes the transition
		 */
		template <class EventType, typename std::enable_if<std::is_base_of<event, EventType>::value, int>::type = 0>
		void addTransition(
			State *s,
			typename SubStateMachine<event>::action a = typename SubStateMachine<event>::action(),
			typename SubStateMachine<event>::guard g = typename SubStateMachine<event>::guard()
			)
		{
			m_Transitions[std::type_index(typeid(EventType))].push_back(std::tie(s, a, g));
		}

	protected:
		/**
		 * @brief      Internal function called on entry. Automatically call onEntry.
		 *
		 * @param[in]  e     The event that lead to this state entry.
		 */
		void onEntryInternal(std::shared_ptr<event> e)
		{
			if (SubStateMachine<event>::m_InitialState != nullptr) {
				if (not m_Memorize || SubStateMachine<event>::m_CurrentState == nullptr ) {
					SubStateMachine<event>::m_CurrentState = SubStateMachine<event>::m_InitialState;
				}
				SubStateMachine<event>::m_CurrentState->onEntryInternal(e);
			}
			onEntry(e);
		}
		/**
		 * @brief      Internal function called on entry. Automatically call onExit.
		 *
		 * @param[in]  e     The event that lead to this state exit.
		 */
		void onExitInternal(std::shared_ptr<event> e)
		{
			if (SubStateMachine<event>::m_CurrentState != nullptr) {
				SubStateMachine<event>::m_CurrentState->onExitInternal(e);
			}
			onExit(e);
		}

	protected:
		bool												m_Memorize; ///< Should the state remember the current substate on exit or be resetted to the initial state.
		std::map<std::type_index,
			std::list<std::tuple<State*,
				typename SubStateMachine<event>::action,
				typename SubStateMachine<event>::guard>>>	m_Transitions; ///< Container holding the transitions

	};

}