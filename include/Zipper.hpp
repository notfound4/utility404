#pragma once

#include <functional>
#include <tuple>
#include <utility>


/**************************** 
// declaration helper for iterator and reference types
****************************/
template <typename T>
using iter_t = decltype(std::declval<T>().begin());

template <typename T>
using ref_t = decltype(*std::declval<iter_t<T>>());


/**************************** 
// wrap into a reference_wrapper if needed
****************************/
template <typename T>
std::reference_wrapper<T> wrapper(T &t) {
	return std::ref(t);
}

template <typename T>
T wrapper(T &&t) {
	return std::move(t);
}

template <class T>
struct special_wrapper
{
	using type = typename std::remove_reference<T>::type;
};

template <class T>
struct special_wrapper<T&>
{
	using type = T&;
};

template <class T>
using special_wrapper_t = typename special_wrapper<T>::type;


/***************************
// increment every element in a tuple (that is referenced)
***************************/
template <class... Ts, std::size_t... Is>
void increment(std::tuple<Ts...>& tpl, std::index_sequence<Is...> ) {
	((void(++std::get<Is>(tpl))), ...);
}

template <class... Ts>
void increment(std::tuple<Ts...>& tpl) {
    increment(tpl, std::index_sequence_for<Ts...>{});
}


/**************************** 
// check equality of a tuple
****************************/
template<typename... Ts, std::size_t... Is>
inline bool not_equal_tuples( const std::tuple<Ts...>& t1,  const std::tuple<Ts...>& t2,
	std::index_sequence<Is...> )
{
	return ((std::get<Is>(t1) != std::get<Is>(t2)) and ...);
}

template<typename... Ts>
inline bool not_equal_tuples( const std::tuple<Ts...>& t1,  const std::tuple<Ts...>& t2 )
{
    return not_equal_tuples(t1, t2, std::index_sequence_for<Ts...>{});
}


/**************************** 
// dereference a subset of elements of a tuple (dereferencing the iterators)
****************************/
template <typename Tuple, std::size_t... indices>
auto dereference_subset(const Tuple& tpl, std::index_sequence<indices...>)
-> decltype(std::tie(*std::get<indices>(tpl)...))
{
	return std::tie(*std::get<indices>(tpl)...);
}

/**************************** 
// dereference every element of a tuple (applying operator* to each element,
// and returning the tuple)
****************************/
template<typename... Ts>
inline auto
dereference_tuple(const std::tuple<Ts...>& t1) -> decltype(
	dereference_subset( std::tuple<Ts...>(), std::index_sequence_for<Ts...>{})
	)
{
	return dereference_subset( t1, std::index_sequence_for<Ts...>{});
}


/**************************** 
// Get the begin iterator of a subset of elements of a tuple
****************************/
template <typename Tuple, std::size_t... indices>
auto begin_subset(Tuple& tpl, std::index_sequence<indices...>)
-> decltype(std::make_tuple(std::get<indices>(tpl).begin()...))
{
	return std::make_tuple(std::get<indices>(tpl).begin()...);
}

/**************************** 
// Get the begin iterator of every elements
****************************/
template<typename... Ts>
inline std::tuple<iter_t<Ts>...>
begin_tuple(std::tuple<Ts...>& t1)
{
	return begin_subset( t1, std::index_sequence_for<Ts...>{});
}

/**************************** 
// Get the end iterator of a subset of elements of a tuple
****************************/
template <typename Tuple, std::size_t... indices>
auto end_subset(Tuple& tpl, std::index_sequence<indices...>)
-> decltype(std::make_tuple(std::get<indices>(tpl).end()...))
{
	return std::make_tuple(std::get<indices>(tpl).end()...);
}

/**************************** 
// Get the end iterator of every elements
****************************/
template<typename... Ts>
inline std::tuple<iter_t<Ts>...>
end_tuple(std::tuple<Ts...>& t1)
{
	return end_subset( t1, std::index_sequence_for<Ts...>{});
}


/**************************** 
// main class for zipping
****************************/
template< typename... Ts >
class zipper
{
    static_assert(sizeof...(Ts) > 0, "!");
public:

	class iterator : std::iterator<std::forward_iterator_tag, std::tuple<ref_t<Ts>...> >
	{
	protected:
		std::tuple<iter_t<Ts>...> current;
	public:

		explicit iterator( iter_t<Ts>... s2 ) : 
		current(s2...) {};

		iterator( const iterator& rhs ) :  current(rhs.current) {};

		iterator& operator++() {
			increment(current);
			return *this;
		}

		iterator operator++(int) {
			auto a = *this;
			increment(current);
			return a;
		}

		bool operator!=( const iterator& rhs ) {
			return not_equal_tuples(current, rhs.current);
		}

		bool operator==( const iterator& rhs ) {
			return !(*this != current);
		}

		std::tuple<ref_t<Ts>...> operator*() {
			return dereference_tuple(current);
		}
	};


	explicit zipper(Ts&&... containers)
	: containers_(wrapper(std::forward<Ts>(containers))...),
	begin_(std::make_from_tuple<iterator>(begin_tuple(containers_))),
	end_(std::make_from_tuple<iterator>(end_tuple(containers_)))
	{ }

	zipper(const zipper& a) = default;

	zipper& operator=(const zipper& rhs) = default;

	zipper<Ts...>::iterator begin() {
		return begin_;
	}

	zipper<Ts...>::iterator end() {
		return end_;
	}

	std::tuple<special_wrapper_t<Ts>...> containers_;
	zipper<Ts...>::iterator begin_;
	zipper<Ts...>::iterator end_;
};


//allows template type deduction for zipper:
template <class... Types>
zipper<Types...> zip(Types&&... args)
{
	return zipper<Types...>(std::forward<Types>(args)...);
}
