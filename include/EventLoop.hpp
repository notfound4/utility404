#pragma once

#include "DispatchQueue.hpp"
#include "EventHandler.hpp"
#include "FiniteStateMachine.hpp"

namespace utility404 {

	/**
	 * @brief      Class for event loop.
	 *
	 *             It is a singleton holding a dispatch queue. Usually, it
	 *             should not be used directly, but instead should be copied in
	 *             the device library and renamed. Then, a new macros header
	 *             file should created containing only 2 includes : first, the
	 *             new event loop file, then the <CODE>Macros.h</CODE> file.
	 *
	 * @tparam     T     Name of the class inheriting from event loop
	 */
	template <class event, class T>
	class EventLoop : public DispatchQueue, public EventHandler<event>
	{
	public:
		/**
		 * @brief      Destroys the object.
		 */
		~EventLoop() = default;

		/**
		 * @brief      Get the instance.
		 *
		 * @return     the instance.
		 */
		static T& Get() {
			static T el;
			// Dummy line so that Get() does not get optimized away.
			// This is important for the thread safety of the initialization.
			volatile int dummy{};
			return el;
		}

		/**
		 * @brief      Posts an event.
		 *
		 * @param[in]  e     A smart pointer to the event to post.
		 */
		void postEvent(std::shared_ptr<event> e)
		{
			dispatch([&, e](){EventHandler<event>::postEvent(e);});
		}

		template <class EventType, typename std::enable_if<std::is_base_of<event, EventType>::value, int>::type = 0>
		void registerFSM(StateMachine<EventType> &sm)
		{
			EventHandler<event>::registerObserver(std::function<void (std::shared_ptr<EventType>)>([&](std::shared_ptr<EventType> e){sm.processEvent(e);}));
		}

	protected:
		EventLoop() = default;
		EventLoop(const EventLoop&) = delete;
		EventLoop(EventLoop&&) = delete;
		EventLoop& operator=(const EventLoop&) = delete;
		EventLoop& operator=(EventLoop&&) = delete;
	};

}